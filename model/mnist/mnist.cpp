#include <fstream>
#include <stdexcept>
#include <algorithm>
#include <dnn/network.h>
#include <dnn/layers/input_layer.h>

#include "mnist.h"
#include "dnn/utility/logger.h"

using namespace dnn;

template<typename T>
T ReverseEndian(T p) {
    std::reverse(reinterpret_cast<char *> (&p), reinterpret_cast<char *>(&p) + sizeof(T));
    return p;
}

inline bool IsLittleEndian() {
    int x = 1;
    return *(char *) &x != 0;
}


/**
 * @brief Train Mnist classification.
 */
void Mnist::Train() {
    Logger::SetLogLevel(LogLevel::DEBUG);

    LOG_INFO("Loading data....");
    LoadMnistImage();
    LoadMnistLabel();

    LOG_INFO("Shuffle data");
    ShuffleData();

    LOG_INFO("Split train and validate data.");
    // Train
    std::vector<Image> train_images(static_cast<size_t>(images_.size() * 0.9f));
    std::vector<Label> train_labels(static_cast<size_t>(labels_.size() * 0.9f));
    std::copy(images_.begin(), images_.begin() + train_images.size(), train_images.begin());
    std::copy(labels_.begin(), labels_.begin() + train_labels.size(), train_labels.begin());
    // Label
    std::vector<Image> validate_images(images_.size() - train_images.size());
    std::vector<Label> validate_labels(labels_.size() - train_labels.size());
    std::copy(images_.begin() + train_images.size(), images_.end(), validate_images.begin());
    std::copy(labels_.begin() + train_labels.size(), labels_.end(), validate_labels.begin());
    LOG_INFO("Prepare date done. train set size %d, validate size %d", train_labels.size(), validate_labels.size());

    float learning_rate = 0.1f;
    const float decay_rate = 0.8f;  // TODO: ?
    const float min_learning_rate = 0.001f;
    const size_t test_after_batch = 50;
    const size_t max_batch = 10000;
    const size_t max_epoch = 5;
    const size_t batch = 128;
    const size_t image_channels = this->images_[0].channel;
    const size_t image_width = this->images_[0].width;
    const size_t image_height = this->images_[0].height;
    LOG_INFO("Set parameters done, image_height=", image_height, ", image_width=", image_width);

    Network<double> network = BuildConvNet(batch, image_channels, image_height, image_width);
}

void Mnist::Test() {

}

//const std::string DATA_PATH = "/home/zhongcy/opt/fly-dnn/model/mnist/data/";
const std::string DATA_PATH = "../model/mnist/data/";

Mnist::Mnist(const std::string &modelFile, const std::string &trainImage, const std::string &trainLabel) :
        model_file_(modelFile), train_image_(trainImage), train_label_(trainLabel) {}

Mnist::Mnist() : train_image_(DATA_PATH + "train-images.idx3-ubyte"),
                 train_label_(DATA_PATH + "train-labels.idx1-ubyte") {}

/**
 * @brief Load mnist images.
 */
bool Mnist::LoadMnistImage() {
    images_.clear();

    std::ifstream ifs(train_image_, std::ios::binary);
    const bool is_little_endian = IsLittleEndian();
    //count
    uint32_t image_count = CheckAndCount(ifs, 0x00000803, is_little_endian);

    uint32_t width = 0, height = 0;
    //image property
    ifs.read((char *) &height, sizeof(height));
    ifs.read((char *) &width, sizeof(width));
    if(is_little_endian){
        height = ReverseEndian<uint32_t>(height);
        width = ReverseEndian<uint32_t>(width);
    }
    LOG_INFO("data count:", image_count, height, width);

    for (uint32_t i = 0; i < image_count; i++) {
        Image image;
        image.channel = 1;
        image.height = height;
        image.width = width;
        image.data.resize(height * width);
        ifs.read((char *) &image.data[0], width * height);
        images_.push_back(image);
    }
    LOG_INFO("Load image success");

    return true;
}

/**
 * @brief Load mnist labels.
 */
bool Mnist::LoadMnistLabel() {
    labels_.clear();
    std::ifstream ifs(train_label_, std::ios::binary);
    const bool is_little_endian = IsLittleEndian();
    uint32_t label_count = CheckAndCount(ifs, 0x00000801, is_little_endian);

    //labels_
    for (uint32_t i = 0; i < label_count; i++) {
        Label label;
        ifs.read((char *) &label.data, sizeof(label.data));
        labels_.push_back(label);
    }
    return true;
}

/**
 * @brief Check endian and read data count.
 * @param ifs File stream
 * @param file_magic_number File magic number
 * @return File data count
 */
uint32_t Mnist::CheckAndCount(std::ifstream &ifs, uint32_t file_magic_number, bool is_little_endian) {
    if (!ifs.is_open()) {
        throw std::ifstream::failure("Can not open file");
    }

    uint32_t magic_number;
    ifs.read((char *) &magic_number, sizeof(magic_number));
    if (is_little_endian) magic_number = ReverseEndian<uint32_t>(magic_number);
    ASSERT(magic_number == file_magic_number, "File magic number error");

    //count
    uint32_t image_count = 0;
    ifs.read((char *) &image_count, sizeof(image_count));
    if(is_little_endian) return ReverseEndian<uint32_t>(image_count);
    return image_count;
}

/**
 * @brief Random shuffle image and label data.
 */
void Mnist::ShuffleData() {
    ASSERT(images_.size() == labels_.size(), "Image and label do not match.");
    std::vector<size_t> index_array;
    for (size_t i = 0; i < images_.size(); i++) {
        index_array.push_back(i);
    }

    std::random_shuffle(index_array.begin(), index_array.end());

    std::vector<Image> shuffled_images(images_.size());
    std::vector<Label> shuffled_labels(labels_.size());
    for (size_t i = 0; i < images_.size(); i++) {
        const size_t src_index = i;
        const size_t dst_index = index_array[i];
        shuffled_images[src_index] = images_[dst_index];
        shuffled_labels[src_index] = labels_[dst_index];
    }
    images_ = shuffled_images;
    labels_ = shuffled_labels;
}

Network<double> Mnist::BuildConvNet(const size_t batch, const size_t channels, const size_t height, const size_t width) {
    Network<double> network;
    Shape shape(batch, channels, height, width);
    network.SetInputSize(shape);

    // Input layer
    AddInputLayer(network);

    AddConvLayer(network, 6, 1);
    
    return network;
}

void Mnist::AddInputLayer(Network<double>& network){
    std::shared_ptr<dnn::InputLayer<double>> input_layer(std::make_shared<dnn::InputLayer<double>>());
    network.AddLayer(input_layer);
}


void Mnist::AddActiveLayer(Network<double> &network) {

}

void Mnist::AddConvLayer(Network<double> &network, const int number, const int channel) {

}

void Mnist::AddPoolLayer(Network<double> &network) {

}

void Mnist::AddFCLayer(Network<double> &network) {

}

void Mnist::AddSoftMaxLayer(Network<double> &network) {

}


