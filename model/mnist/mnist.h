#ifndef FLY_DNN_MNIST_H
#define FLY_DNN_MNIST_H

#include <string>
#include <vector>
#include <cstdint>

#include "dnn/network.h"

using namespace dnn;

/**
 * @brief Image data
 */
struct Image{
    size_t width, height, channel;
    std::vector<uint8_t> data;
};

/**
 * @brief Label data
 */
struct Label{
    uint8_t data;
};

/**
 * @brief Mnist operation class
 */
class Mnist{
public:
    Mnist();

    Mnist(const std::string &modelFile, const std::string &trainImage, const std::string &trainLabel);

    bool LoadMnistImage();

    bool LoadMnistLabel();

    void ShuffleData();

    void Train();

    void Test();

private:
    std::string model_file_;

    std::string train_image_;

    std::string train_label_;

    std::vector<Image> images_;

    std::vector<Label> labels_;

    uint32_t CheckAndCount(std::ifstream& ifs,  uint32_t magic_number, bool is_little_endian);

    Network<double> BuildConvNet(const size_t batch, const size_t channels, const size_t height, const size_t width);

    static void AddInputLayer(Network<double>& network);

    void AddActiveLayer(Network<double>& network);

    void AddConvLayer(Network<double>& network, int number, int channel);

    void AddPoolLayer(Network<double>& network);

    void AddFCLayer(Network<double>& network);

    void AddSoftMaxLayer(Network<double>& network);
};



#endif  // FLY_DNN_MNIST_H