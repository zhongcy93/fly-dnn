/*
Copyright [2019] [kode]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "dnn/tensor.h"
#include "dnn/utility/util.h"
#include <gtest/gtest.h>
#include <memory>

namespace dnn {

    /*
    * Test for Tensor class
    */
    template<typename DType>
    class TensorTest : public testing::Test {
    public:
        TensorTest() : tensor_(new Tensor<DType>()), shaped_tensor_(new Tensor<DType>(2, 3, 4, 5)) {
            std::vector<DType> v1;
            std::vector<DType> v2(120, 1.0);
            tensor_->Place(std::move(v1));
            shaped_tensor_->Place(std::move(v2));
        }

        Tensor<DType> * tensor_;
        Tensor<DType> *const shaped_tensor_;
        std::shared_ptr<Tensor<DType>> tensor_ptr_src = std::make_shared<Tensor<DType>>();

        std::shared_ptr<Tensor<DType>> tensor_ptr_target = std::make_shared<Tensor<DType>>();
    };

    typedef testing::Types<float> TensorType;
    TYPED_TEST_CASE(TensorTest, TensorType);

    TYPED_TEST(TensorTest, INIT) {
        EXPECT_EQ(this->shaped_tensor_->Nums(), 2);
        EXPECT_EQ(this->shaped_tensor_->Channel(), 3);
        EXPECT_EQ(this->shaped_tensor_->Height(), 4);
        EXPECT_EQ(this->shaped_tensor_->Width(), 5);

    }

    TYPED_TEST(TensorTest, DATA){
        this->tensor_->Reshape(1, 2, 3, 4);
        vector<float> v(24, 2.0);
        this->tensor_->Place(std::move(v));
        EXPECT_EQ((*this->tensor_)(0, 0 , 1, 2), 2.0);
    }

    TYPED_TEST(TensorTest, FILL){
        this->tensor_ptr_src->Reshape(1, 2, 3, 4);
        this->tensor_ptr_src->FillData(3.14);
        for(const auto& v : *(this->tensor_ptr_src->GetData())){
            EXPECT_FLOAT_EQ(3.14, v);
        }
        this->tensor_ptr_src->CloneTo(this->tensor_ptr_target);
        for(const auto& v : *(this->tensor_ptr_target->GetData())){
            EXPECT_FLOAT_EQ(3.14, v);
        }
    }
} // namespace dnn