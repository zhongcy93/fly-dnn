//
// Created by 钟乘永 on 2019-08-31.
//
#include "dnn/utility/thread_pool.h"
#include <gtest/gtest.h>

TEST(THREAD_POOL, CAL){
    int res = 0;
    auto cal = [&](const size_t start, const size_t stop){
        for(size_t i = start; i <= stop; i++){
            res += i;
        }
    };
    dnn::DispatcherWork(cal, 100);
    EXPECT_EQ(5050, res);
}
