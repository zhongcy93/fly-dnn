//
// Created by 钟乘永 on 2019-08-24.
//

#include <memory>
#include <gtest/gtest.h>

using namespace std;

TEST(BASE, SHARED_PTR) {
    vector<int> *vp = new vector<int>(10, 1);
    cout << vp->at(0) << endl;
    cout << (*vp)[1] << endl;
    std::shared_ptr<vector<int>> sv(vp);
    cout << vp->at(0) << endl;
    std::shared_ptr<int> sp = std::make_shared<int>();
    cout << "shared_ptr value:" << *sp.get() << endl;

    /*
     * make_shared:
     * 1. empty parameter
     * 2. RValue
     */
    std::shared_ptr<vector<int>> sv2 = std::make_shared<vector<int>>();
    vector<int> v(10, 1);
    std::shared_ptr<vector<int>> sv3 = std::make_shared<vector<int>>(v);
    std::shared_ptr<vector<int>> sv4 = std::make_shared<vector<int>>(std::move(v));

    std::shared_ptr<vector<int>> sv5;
    assert(sv5 == nullptr);
    assert(sv5.get() == nullptr);
    assert(sv3 != nullptr);

    std::shared_ptr<vector<int>> sv6(new vector<int>);
    assert(sv6 != nullptr);
}

TEST(BASE, VECTOR_TEST){
    vector<int> v{};
//    v.resize(2);
    v.assign(2, 1);
    for(auto const& d : v){
        EXPECT_EQ(1, d);
    }
}
