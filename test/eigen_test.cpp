////
//// Created by 钟乘永 on 2019-05-18.
////
//
//#include <Eigen/Dense>
//#include <gtest/gtest.h>
//#include <iostream>
//
//using Eigen::MatrixXd;
//using namespace std;
//using namespace Eigen;
//namespace dnn{
//    TEST(EIGEN, SIMPLE){
//        MatrixXd m(2,2);
//        m(0, 0) = 3;
//        m(1, 0) = 2.5;
//        m(0, 1) = -1;
//        m(1, 1) = m(1, 0) + m(0, 1);
//        EXPECT_EQ(m(1, 1), 1.5);
//    }
//
//    // Size set at run time
//    TEST(Eigen, VECTOR){
//        MatrixXd m = MatrixXd::Random(3, 3);
//        m = (m + MatrixXd::Constant(3, 3, 1.2)) * 50;
//        std::cout << "m=" << endl << m << endl;
//        VectorXd v(3);
//        v << 1, 2, 3;
//        cout << "m*v=" << endl << m * v << endl;
//    }
//
//    // Size set at compile time
//    TEST(Eigen, COMPILE){
//        Matrix3d m = Matrix3d::Random();
//        m = (m + Matrix3d::Constant(3, 3, 1.2)) * 50;
//        std::cout << "m=" << endl << m << endl;
//        VectorXd v(3);
//        v << 1, 2, 3;
//        cout << "m*v=" << endl << m * v << endl;
//    }
//}
