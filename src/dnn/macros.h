/*
Copyright [2019] [kode]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef FLY_DNN_MACROS_H
#define FLY_DNN_MACROS_H

#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/seq/enum.hpp>
#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each_product.hpp>
#include <boost/preprocessor/seq/to_tuple.hpp>
#include <boost/preprocessor/tuple/to_seq.hpp>

#ifndef INSTANTIATE_CLASS_HELPER
#define INSTANTIATE_CLASS_HELPER(R, CLASSNAME, T)\
template class CLASSNAME<T>;
#endif

#ifndef INSTANTIATE_CLASS_NT
#define INSTANTIATE_CLASS_NT(CLASSNAME, T) \
BOOST_PP_SEQ_FOR_EACH(INSTANTIATE_CLASS_HELPER, CLASSNAME, T);
#endif

#ifndef ENUM_MAP
#define ENUM_MAP(e, m) m[e] = #e
#endif

#ifndef INSTANTIATE_CLASS_3T
#define INSTANTIATE_CLASS_3T(CLASSNAME, T1, T2, T3) \
template class CLASSNAME<T1, T2, T3>;
#endif

#ifndef INSTANTIATE_CLASS_1F
#define INSTANTIATE_CLASS_1F(CLASSNAME)\
template class CLASSNAME<float>;
#endif

#ifndef INSTANTIATE_CLASS_1D
#define INSTANTIATE_CLASS_1D(CLASSNAME)\
template class CLASSNAME<double>;
#endif


#endif //FLY_DNN_MACROS_H
