/*
Copyright [2019] [kode]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef FLY_DNN_CONVOLUTION_LAYER_H
#define FLY_DNN_CONVOLUTION_LAYER_H

#include "layer.h"
#include "dnn/utility/util.h"
#include "dnn/params/convolution_param.h"
#include <vector>

namespace dnn {
    template<typename DType>
    class ConvolutionLayer : public Layer<DType> {
    public:
        explicit ConvolutionLayer(size_t in_width, size_t in_height, size_t window_size, size_t in_channels,
                                  size_t out_channels, Padding padding = Padding::VALID, bool has_bias = true,
                                  size_t w_stride = 1, size_t h_stride = 1, size_t w_dilation = 1,
                                  size_t h_dilation = 1) {
            conv_set_param(in_width, in_height, window_size, in_channels, out_channels, padding,
                           has_bias, w_stride, h_stride,
                           w_dilation, h_dilation);
        }

        explicit ConvolutionLayer() = default;;

        void
        conv_set_param(size_t int_width, size_t in_height, size_t window_size, size_t in_channels, size_t out_channel,
                       Padding padding, bool has_bias, size_t w_stride, size_t h_stride, size_t w_dilation,
                       size_t h_dilation) {
            conv_param_.kernel_w_ = window_size;
            conv_param_.kernel_h_ = window_size;
            conv_param_.out_channel_ = out_channel;
            conv_param_.padding_ = padding;
            if (padding == Padding::VALID) {
                conv_param_.pad_ = 0;
            } else {
                // TODO
            }
            conv_param_.stride_w_ = w_stride;
            conv_param_.stride_h_ = h_stride;
            conv_param_.dilation_w_ = w_dilation;
            conv_param_.dilation_h_ = h_dilation;
            conv_param_.has_bias_ = has_bias;
        }

        void Forward(const p_tensor_vec<DType> &bottom,
                     p_tensor_vec<DType> &top) override;

        void Backward(const p_tensor_vec<DType> &top,
                      p_tensor_vec<DType> &bottom,
                      p_tensor_vec<DType> &bottom_diff,
                      const p_tensor_vec<DType> &top_diff) override;

        string Type() override {
            return "Convolution";
        };


    private:
        /*
         * Note: Different with EasyCNN here, DO NOT use shared_ptr here since redundant util now.
         */
        ConvolutionParam conv_param_;
        Tensor<DType> weight_;
        Tensor<DType> bias_;
        Tensor<DType> weight_grad_;
        Tensor<DType> bias_grad;

        /**
         * Convolution calculation.
         */

        void Convolution2d(std::shared_ptr<Tensor<DType>> input, std::shared_ptr<Tensor<DType>> output);
    };
}


#endif //FLY_DNN_CONVOLUTION_LAYER_H
