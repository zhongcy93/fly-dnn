//
// Created by 钟乘永 on 2019-03-24.
//

#ifndef FLY_DNN_LAYER_H
#define FLY_DNN_LAYER_H

#include <vector>
#include <memory>
#include "dnn/utility/util.h"
#include "dnn/tensor.h"
#include "dnn/utility/logger.h"

using std::string;

namespace dnn {
    template<typename DType>
    class Layer {
    public:
        explicit Layer() = default;

    protected:

        // Layer type
        virtual string Type() = 0;

        virtual string Name() {
            return name_;
        };

        // Set train mode.
        virtual void train() {}

        // Set test mode.
        virtual void test() {}

        virtual void Forward(const p_tensor_vec<DType> &bottom,
                             p_tensor_vec<DType> &top) = 0;

        virtual void Backward(const p_tensor_vec<DType> &top,
                              p_tensor_vec<DType> &bottom,
                              p_tensor_vec<DType> &bottom_diff,
                              const p_tensor_vec<DType> &top_diff) = 0;

        inline Shape GetInputBucketShape() const { return input_shape_; }

        inline Shape GetOutputBucketShape() const { return output_shape_; }

    public:
        // Getter & Setter
        Phase GetPhase() const {
            return phase_;
        }

        void SetPhase(Phase phase) {
            phase_ = phase;
        }

        const Shape &GetInputShape() const {
            return input_shape_;
        }

        void SetInputShape(const Shape &inputShape) {
            input_shape_ = inputShape;
        }

        const Shape &GetOutputShape() const {
            return output_shape_;
        }

        void SetOutputShape(const Shape &outputShape) {
            output_shape_ = outputShape;
        }

        float GetLearningRate() const {
            return learning_rate_;
        }

        void SetLearningRate(float learningRate) {
            learning_rate_ = learningRate;
        }

        const p_tensor_vecs<DType> &GetGradients() const {
            return gradients_;
        }

        const p_tensor_vecs<DType> &GetParams() const {
            return params_;
        }

    protected:
        Phase phase_ = Phase::TRAIN;

        // Layer type, e.g. conv/fc.
        string type_;

        // Layer name.
        string name_;

        Shape input_shape_;

        Shape output_shape_;

        float learning_rate_ = 0.1f;

        /*
         * Use vector<vector<Tensor<DType>*>*> to store all layer's gradient and params
         */
        p_tensor_vecs<DType> gradients_;

        p_tensor_vecs<DType> params_;


        // Bottom layers
        //std::vector<Layer<DType> *> bottom_;

        //// Top layers
        //std::vector<Layer<DType> *> top_;

        //// Record output data for Forward.
        //Tensor<DType> out_data_;

        //// Record gradient for Backward.
        //Tensor<DType> out_grad_;

//        /**
//         * Whether need to check count match when forward.
//         * @param bottom
//         * @return
//         */
//        virtual void CheckForward(const p_tensor_vec<DType> &bottom) {
//            ASSERT(bottom.size() == bottom_.size(), "Forward size unmatch.", name_);
//        }
//
//        /**
//         * Whether need to check count match when backward.
//         * @param top
//         * @return
//         */
//        virtual void CheckBackward(const p_tensor_vec<DType> &top) {
//            ASSERT(top.size() == top_.size(), "Backward size unmatch.", name_);
//        }
    };

    // The layer alias
    typedef std::unique_ptr<Layer<float>> FloatModel;
    typedef std::unique_ptr<Layer<float>> FloatLayerPtr;
} // namespace dnn


#endif //FLY_DNN_LAYER_H
