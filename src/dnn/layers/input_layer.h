//
// Created by 钟乘永 on 2019-07-28.
//

#ifndef FLY_DNN_INPUT_LAYER_H
#define FLY_DNN_INPUT_LAYER_H

#include "layer.h"

namespace dnn {

    template<typename DType>
    class InputLayer : public Layer<DType> {
    public:
        explicit InputLayer(const string &type = "input", const string &name = "input_layer"){
            this->type_ = type;
            this->name_ = name;
        }

        // Layer type
        string Type() override {
            return this->type_;
        };

        string Name() override {
            return this->name_;
        }

        void Forward(const p_tensor_vec<DType> &bottom, p_tensor_vec<DType> &top) override;
        void Backward(const p_tensor_vec<DType> &top, p_tensor_vec<DType> &bottom, p_tensor_vec<DType> &bottom_diff,
                      const p_tensor_vec<DType> &top_diff) override;

    };


};


#endif //FLY_DNN_INPUT_LAYER_H
