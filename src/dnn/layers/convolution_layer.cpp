/*
Copyright [2019] [kode]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "convolution_layer.h"
#include "dnn/macros.h"

namespace dnn {

    /**
     * 备忘：这里bottom数组是用于有多个Layer作为输入的case，
     * 其中每个Tensor都有nxcxhxw个数据
     * @tparam DType
     * @param bottom
     * @param top
     */
    template<typename DType>
    void ConvolutionLayer<DType>::Forward(const p_tensor_vec<DType> &bottom, p_tensor_vec<DType> &top) {
        // CheckForward(bottom);
//        const Shape input_shape = bottom[0]->S

        const size_t bottom_size = bottom.size();
        // Layer loop
        for (size_t i = 0; i < bottom_size; i++) {
            Convolution2d(bottom[i], top[i]);
        }
    }

    template<typename DType>
    void ConvolutionLayer<DType>::Backward(const p_tensor_vec<DType> &top, p_tensor_vec<DType> &bottom,
                                           p_tensor_vec<DType> &bottom_diff,
                                           const p_tensor_vec<DType> &top_diff) {
        assert(this->phase_ == Phase::TRAIN);

        // Just calculate right when only one layer

        auto top_tensor = top[0];
        auto top_diff_tensor = top_diff[0];
        for(size_t i = 0; i < top.size(); i++){
            auto bottom_tensor = bottom[i];
            auto bottom_diff_tensor = bottom_diff[i];
            const Shape bottom_shape = bottom_tensor->GetShape();
            const Shape top_shape = top_tensor->GetShape();
            const Shape bottom_diff_shape = bottom_diff_tensor->GetShape();
            const Shape top_diff_shape = top_diff_tensor->GetShape();
            const Shape bias_shape = this->bias_.GetShape();

            const vector<DType>* bottom_data = bottom_tensor->GetData();
            const vector<DType>* top_data = top_tensor->GetData();
            vector<DType>* bottom_diff_data = bottom_diff_tensor->GetData();
            const vector<DType>* top_diff_data = top_diff_tensor->GetData();
            const vector<DType>* bias_data = bias_.GetData();
            const vector<DType>* weight_data = weight_.GetData();

            assert(bottom_diff_shape == bottom_shape);

            bottom_diff_tensor->FillData((DType)0);
        }
    }

    template<typename DType>
    void ConvolutionLayer<DType>::Convolution2d(std::shared_ptr<Tensor<DType>> input,
                                                std::shared_ptr<Tensor<DType>> output) {
        const Shape input_shape(input->Nums(), input->Channel(), input->Height(), input->Width());
        const Shape kernel_shape(conv_param_.out_channel_, input->Channel(), conv_param_.kernel_h_,
                                 conv_param_.kernel_w_);

        const Shape out_shape(input->Nums(), conv_param_.out_channel_, output->Height(), output->Width());
        // Do conv, PAD valid.
        for (size_t in = 0; in < input_shape.nums_; in++) {
            for (size_t ic = 0; ic < out_shape.channel_; ic++) {
                for (size_t ih = 0; ih < out_shape.height_; ih++) {
                    for (size_t iw = 0; iw < out_shape.width_; iw++) {
                        const size_t width_start = iw * conv_param_.stride_w_;
                        const size_t height_start = ih * conv_param_.stride_h_;
                        DType sum = 0;
                        for (size_t kc = 0; kc < kernel_shape.channel_; kc++) {
                            for (size_t kh = 0; kh < kernel_shape.height_; kh++) {
                                for (size_t kw = 0; kw < kernel_shape.width_; kw++) {
                                    const size_t input_index = input->offset(in, kc, height_start + kh,
                                                                             width_start + kw);
                                    const size_t kernel_index = weight_.offset(ic, kc, kh, kw);
                                    sum += input->at(input_index) * weight_.at(kernel_index);
                                }
                            }
                        }
                        if (conv_param_.has_bias_) {
                            sum += bias_(ic);
                        }
                        const size_t output_index = output->offset(in, ic, ih, iw);
                        output->at(output_index) = sum;
                    }
                }
            }
        }
    }


    INSTANTIATE_CLASS_1D(ConvolutionLayer);

    INSTANTIATE_CLASS_1F(ConvolutionLayer);
} // namespace dnn
