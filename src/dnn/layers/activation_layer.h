//
// Created by 钟乘永 on 2019-07-28.
//

#ifndef FLY_DNN_ACTIVATION_LAYER_H
#define FLY_DNN_ACTIVATION_LAYER_H

#include "layer.h"

namespace dnn{
    template <typename DType>
    class ActivationLayer : public Layer<DType> {

    };


    template <typename DType>
    class SigmoidLayer : public ActivationLayer<DType>{

    };

    template <typename DType>
    class TanhLayer : public ActivationLayer<DType>{

    };

    template <typename DType>
    class  ReluLayer : public ActivationLayer<DType>{

    };
};  // namespace dnn


#endif //FLY_DNN_ACTIVATION_LAYER_H
