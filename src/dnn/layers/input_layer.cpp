//
// Created by 钟乘永 on 2019-07-28.
//

#include "input_layer.h"

namespace dnn {
    template<typename DType>
    void InputLayer<DType>::Forward(const p_tensor_vec<DType> &bottom, p_tensor_vec<DType> &top) {
        for(const std::shared_ptr<Tensor<DType>>& tensor_ptr : bottom){
            // TODO: Is this efficient?? move ptr to top layer
            top.push_back(tensor_ptr);
        }
    }

    template<typename DType>
    void InputLayer<DType>::Backward(const p_tensor_vec<DType> &top, p_tensor_vec<DType> &bottom,
                                     p_tensor_vec<DType> &bottom_diff, const p_tensor_vec<DType> &top_diff) {

    }

    INSTANTIATE_CLASS_1D(InputLayer)

    INSTANTIATE_CLASS_1F(InputLayer)
}