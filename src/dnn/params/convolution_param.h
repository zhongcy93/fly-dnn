//
// Created by 钟乘永 on 2019-03-24.
//

#ifndef FLY_DNN_CONVOLUTION_PARAM_H
#define FLY_DNN_CONVOLUTION_PARAM_H

#include "dnn/utility/util.h"
#include "params.h"
#include "dnn/utility/logger.h"
#include <iostream>

/**
 * Convolution param for convolution layer.
 */
namespace dnn {
    class ConvolutionParam : public Params {
    public:

        explicit ConvolutionParam(size_t kernelW, size_t kernelH, size_t outChannel, Padding padding, size_t pad,
                                  size_t strideW, size_t strideH, size_t dilationW, size_t dilationH, bool hasBias)
                : kernel_w_(kernelW),
                  kernel_h_(kernelH),
                  out_channel_(outChannel),
                  padding_(padding),
                  pad_(pad),
                  stride_w_(strideW),
                  stride_h_(strideH),
                  dilation_w_(dilationW),
                  dilation_h_(dilationH),
                  has_bias_(hasBias) {}

        explicit ConvolutionParam() = default;

        size_t kernel_w_;
        size_t kernel_h_;
        size_t out_channel_;
        Padding padding_;
        size_t pad_;
        size_t stride_w_;
        size_t stride_h_;
        size_t dilation_w_;
        size_t dilation_h_;
        bool has_bias_;


        friend std::ostream &operator<<(std::ostream &os, ConvolutionParam &param) {
            os << "kernel_w_:   " << param.kernel_w_ << "\n";
            os << "kernel_h_:   " << param.kernel_h_ << "\n";
            os << "padding:    " << param.padding_ << "\n";
            os << "out_channel:" << param.out_channel_ << "\n";
            os << "pad:        " << param.pad_ << "\n";
            os << "stride_w:   " << param.stride_w_ << "\n";
            os << "stride_h:   " << param.stride_h_ << "\n";
            os << "dilation_w: " << param.dilation_w_ << "\n";
            os << "dilation_h: " << param.dilation_h_ << "\n";
            os << "has_bias:   " << param.has_bias_ << "\n";
            return os;
        }

        ConvolutionParam &Conv() override {
            return *static_cast<ConvolutionParam *>(this);
        }

    };
} // namespace dnn


#endif //FLY_DNN_CONVOLUTION_PARAM_H
