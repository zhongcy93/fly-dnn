//
// Created by kode on 4/18/19.
//

#ifndef FLY_DNN_LOGGER_H
#define FLY_DNN_LOGGER_H

#include <string>
#include <iostream>
#include <fstream>
#include <chrono>

namespace dnn {
    enum LogLevel {
        DEBUG,
        INFO,
        WARNING,
        ERROR,
        FATAL
    };

    enum LogType{
        FILE,
        CONSOLE
    };

    class Logger {
    public:
        Logger();

        ~Logger() = default;

        template<typename... Args>
        static void Debug(const std::string& format, Args&&... args) {
            std::string info(Time());
            info.append("[Debug]").append(format);
            Write(info, std::forward<Args>(args)...);
        };

        template<typename... Args>
        static void Info(const std::string& format, Args&&... args) {
            std::string info(Time());
            info.append("[Info]").append(format);
            Write(info, std::forward<Args>(args)...);
        };

        template<typename... Args>
        static void Warning(const std::string& format, Args&&... args) {
            std::string info(Time());
            info.append("[Warning]").append(format);
            Write(info, std::forward<Args>(args)...);
        };

        template<typename... Args>
        static void Error(const std::string& format, Args&&... args) {
            std::string info(Time());
            info.append("[Error]").append(format);
            Write(info, std::forward<Args>(args)...);        };

        template<typename... Args>
        static void Fatal(const std::string& format, Args&&... args) {
            std::string info(Time());
            info.append("[Fatal]").append(format);
            Write(info, std::forward<Args>(args)...);
            exit(1);
        };

        // Getter
        static LogLevel& Level(){
            static LogLevel level(LogLevel::INFO);
            return level;
        }

        // Setter
        static void SetLogLevel(LogLevel logLevel){
            Level() = logLevel;
        }
    private:

        // Private Getter
        static LogType &Type(){
            static LogType log_type(LogType::CONSOLE);
            return log_type;
        }

        static std::string& File(){
            static std::string log_file("dnn-log.txt");
            return log_file;
        }

        static std::ostream& Stream(){
            if (Type() == LogType::CONSOLE) {
                return std::cout;
            }else{
                static std::ofstream out_file(File(), std::ofstream::app);
                return out_file;
            }
        }

        // Write

        template<typename T>
        static void Write(T &&t){
            Stream() << t << std::endl;
        }

        /**
         * TODO: Use "," to concat string is not best achieve.
         */
        template<typename T, typename... Args>
        static void Write(T &&t, Args &&... args){
            Stream() << t << " ";
            Write(std::forward<Args>(args)...);
        }

        static std::string Time(){
            using std::chrono::system_clock;
            auto tp = system_clock::now();
            auto time = system_clock::to_time_t(tp);
            std::string format("[");
            std::string str_time = std::ctime(&time);
            str_time.pop_back();
            format.append(str_time);
            format.push_back(']');
            return format;
        }

    };

//    static Logger *logger = new Logger();

#define LOG_CHECK(level) if (Logger::Level() <= level)

#define LOG_DEBUG(...)         LOG_CHECK(LogLevel::DEBUG)   Logger::Debug(__VA_ARGS__)
#define LOG_INFO(...)          LOG_CHECK(LogLevel::INFO)    Logger::Info(__VA_ARGS__)
#define LOG_WARNING(...)       LOG_CHECK(LogLevel::WARNING) Logger::Warnig(__VA_ARGS__)
#define LOG_ERROR(...)         LOG_CHECK(LogLevel::ERROR)   Logger::Error(__VA_ARGS__)
#define LOG_FATAL(...)                                      Logger::Fatal(__VA_ARGS__)

// Define CHECK macro
#ifndef CHECK
#define CHECK(condition)\
        if(!(condition)) Logger::Fatal("Check failed:[" #condition\
        "] in file", __FILE__, "line", __LINE__);
#endif // CHECK

// Define ASSERT macro
#ifndef ASSERT
#define ASSERT(BOOL_SYMBOL, ...) {\
            if(!(BOOL_SYMBOL)){\
                LOG_FATAL( __VA_ARGS__);\
            }\
        }
#endif
}


#endif //FLY_DNN_LOGGER_H
