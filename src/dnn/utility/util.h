//
// Created by 钟乘永 on 2019-03-24.
//

#ifndef FLY_DNN_UTIL_H
#define FLY_DNN_UTIL_H

#include "dnn/macros.h"
#include <iostream>
#include <map>
#include <string>

/**
 * Padding include valid and same.
 */
namespace dnn {
    // Phase for dnn using
    enum class Phase{
        TRAIN,
        TEST
    };

    enum class Padding {
        VALID,
        SAME
    };

    std::ostream &operator<<(std::ostream &ostream, const Padding &padding);


    struct Shape {
        Shape();

        Shape(size_t nums, size_t channel, size_t height, size_t width);

        void Reshape(size_t nums, size_t channel, size_t height, size_t width);

        size_t Size() const{ return nums_ * channel_ * height_ * width_; }
        
        inline bool operator==(const Shape& rhs) const{
            return nums_ == rhs.nums_ && channel_ == rhs.channel_ && height_ && rhs.channel_ && width_ == rhs.width_;
        }
        
        inline bool operator!=(const Shape& rhs) const{
            return !(*this == rhs);
        }
        

        size_t nums_;
        size_t channel_;
        size_t height_;
        size_t width_;

    };
} // namespace dnn


#endif //FLY_DNN_UTIL_H

