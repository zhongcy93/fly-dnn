//
// Created by 钟乘永 on 2019-07-06.
//

#ifndef FLY_DNN_THREAD_POOL_H
#define FLY_DNN_THREAD_POOL_H

#include <functional>
#include <vector>
#include <queue>
#include <thread>
#include <future>
#include <mutex>
#include <condition_variable>
#include <stdexcept>
#include <memory>

namespace dnn {
    class ThreadPool {
    public:
        // Singleton
        static ThreadPool &Instance();

        size_t Size() const;

        void Resize(size_t new_size);

        void ShutDown();

        template<class F, class... Args>
        auto Enqueue(F &&f, Args &&... args) -> std::future<typename std::result_of<F(Args...)>::type>;

    private:
        explicit ThreadPool(size_t threads);

        virtual ~ThreadPool();

        void Startup(size_t threads);

    private:
        // worker threads
        std::vector<std::thread> workers;
        // task queue
        std::queue<std::function<void()>> tasks;

        //synchronization
        std::mutex queue_mutex;
        std::condition_variable condition;
        bool stop = true;
    };


    template<class F, class... Args>
    auto ThreadPool::Enqueue(F &&f, Args &&... args)
    -> std::future<typename std::result_of<F(Args...)>::type> {
        using return_type = typename std::result_of<F(Args...)>::type;

        auto task = std::make_shared<std::packaged_task<return_type()> >(
                std::bind(std::forward<F>(f), std::forward<Args>(args)...)
        );

        std::future<return_type> res = task->get_future();
        std::unique_lock<std::mutex> lock(queue_mutex);

        // don't allow enqueueing after stopping the pool
        if (stop)
            throw std::runtime_error("enqueue on stopped ThreadPool");

        tasks.emplace([task]() { (*task)(); });
        condition.notify_one();
        return res;
    }

    // APIs
    size_t GetThreadNum();

    // Set thread number, and return supported thread number
    size_t SetThreadNum(size_t num);

    // Dispatcher work with threads
    void DispatcherWork(std::function<void(const size_t, const size_t)>, size_t number);
}  // namespace dnn
#endif //FLY_DNN_THREAD_POOL_H
