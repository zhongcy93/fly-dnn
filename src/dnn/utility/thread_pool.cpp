//
// Created by 钟乘永 on 2019-07-06.
//

#include <iostream>
#include <cassert>
#include "thread_pool.h"

namespace dnn {

    ThreadPool &ThreadPool::Instance() {
        static ThreadPool instance(2);
        return instance;
    }

    size_t ThreadPool::Size() const {
        return workers.size();
    }

    void ThreadPool::Resize(const size_t new_size) {
        ShutDown();
        Startup(new_size);
    }

    ThreadPool::ThreadPool(size_t threads) {
        Startup(threads);
    }

    ThreadPool::~ThreadPool() {
        ShutDown();
    }

    void ThreadPool::ShutDown() {
        // Use {} to make variable lifetime in this block
        {
            std::unique_lock<std::mutex> lock(queue_mutex);
            stop = true;
        }
        condition.notify_all();
        for (std::thread &worker : workers) {
            if (worker.joinable()) {
                worker.join();
            }
        }
        workers.clear();
        tasks = decltype(tasks)();
    }

    void ThreadPool::Startup(const size_t threads) {
        stop = false;
        for (size_t i = 0; i < threads; ++i) {
            workers.emplace_back([this] {
                                     for (;;) {
                                         std::function<void()> task;
                                         {
                                             std::unique_lock<std::mutex> lock(this->queue_mutex);
                                             this->condition.wait(lock, [this] {
                                                 return this->stop || !this->tasks.empty();
                                             });
                                             if (this->stop && this->tasks.empty()) return;
                                             task = std::move(this->tasks.front());
                                             this->tasks.pop();
                                         }
                                         task();
                                     }
                                 }

            );
        }
    }


    size_t GetThreadNum() {
        return ThreadPool::Instance().Size();
    }

    size_t SetThreadNum(const size_t num) {
        assert(num > 0);
        if (num != GetThreadNum()) {
            ThreadPool::Instance().Resize(num);
        }
        return GetThreadNum();
    }

    void DispatcherWork(std::function<void(const size_t, const size_t)> func, size_t number) {
        if (number <= 0) return;
        const size_t threads_of_pool = ThreadPool::Instance().Size();
        // Dispatch task to thread pool to run
        if (threads_of_pool <= 1 || number <= 1) {
            func(0, number);
        } else {
            const size_t thread_payload = number / threads_of_pool;
            const size_t remainder_payload = number - thread_payload * threads_of_pool;
            const size_t remainder_idx = remainder_payload;

            size_t start = 0;
            size_t stop = start;
            std::vector<std::future<void>> futures;
            for (size_t i = 0; i < threads_of_pool; ++i) {
                stop = start + thread_payload;
                if (stop >= number) stop = number;
                futures.push_back(ThreadPool::Instance().Enqueue(func, start, stop));
                start = stop + 1;
                if (stop >= number) break;
            }
            for (const auto &future : futures) {
                future.wait();
            }
        }
        std::cout << "Dispatcher completed" << std::endl;
    }
}
