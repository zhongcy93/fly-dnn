//
// Created by 钟乘永 on 2019-04-20.
//
#include "util.h"

namespace dnn {
    std::ostream &operator<<(std::ostream &ostream, const Padding &padding) {
        std::map<Padding, std::string> m;
        ENUM_MAP(Padding::VALID, m);
        ENUM_MAP(Padding::SAME, m);
        return std::cout << m[padding];
    }

    Shape::Shape() : nums_(0), channel_(0), height_(0), width_(0) {

    }

    Shape::Shape(size_t nums, size_t channel, size_t height, size_t width) : nums_(nums), channel_(channel),
                                                                             height_(height), width_(width) {}

    void Shape::Reshape(size_t nums, size_t channel, size_t height, size_t width) {
        nums_ = nums;
        channel_ = channel;
        height_ = height;
        width_ = width;
    }
}