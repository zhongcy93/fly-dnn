#include "loss_function.h"

namespace dnn {
    template<typename DType>
    float
    CrossEntropyFunctor<DType>::GetLoss(const p_tensor_vec<DType> &label_data, const p_tensor_vec<DType> &output_data) {
        return 0;
    }

    template<typename DType>
    float
    CrossEntropyFunctor<DType>::GetDiff(const p_tensor_vec<DType> &label_data, const p_tensor_vec<DType> &output_data) {
        return 0;
    }

    template<typename DType>
    float MSEFunctor<DType>::GetLoss(const p_tensor_vec<DType> &label_data, const p_tensor_vec<DType> &output_data) {
        return 0;
    }

    template<typename DType>
    float MSEFunctor<DType>::GetDiff(const p_tensor_vec<DType> &label_data, const p_tensor_vec<DType> &output_data) {
        return 0;
    }

    INSTANTIATE_CLASS_NT(CrossEntropyFunctor, (float) (double))

    INSTANTIATE_CLASS_NT(MSEFunctor, (float) (double))
}  // namespace dnn
