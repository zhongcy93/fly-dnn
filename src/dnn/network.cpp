#include "network.h"
#include "macros.h"
#include "utility/logger.h"

namespace dnn{
    template<typename DType>
    Network<DType>::Network() {

    }

    template<typename DType>
    Network<DType>::~Network() {

    }

    template<typename DType>
    float Network<DType>::GetLoss(p_tensor_vec<DType> label_data, p_tensor_vec<DType> output_data) {
        return 0;
    }

    template<typename DType>
    void Network<DType>::SetInputSize(Shape shape) {
        LOG_DEBUG("Set network input size");
        ASSERT(shape.nums_ > 0 && shape.channel_ > 0 && shape.height_ > 0 && shape.width_ > 0, "Input shape illegal")

    }

    template<typename DType>
    void Network<DType>::SetLossFunctor(shared_ptr<LossFunctor<DType>> loss_functor) {

    }

    template<typename DType>
    void Network<DType>::SetOptimizer(shared_ptr<Optimizer<DType>> optimizer) {

    }

    template<typename DType>
    void Network<DType>::SetLearningRate(float lr) {

    }

    template<typename DType>
    void Network<DType>::AddLayer(shared_ptr<Layer<DType>> layer) {

    }

    template<typename DType>
    float Network<DType>::TrainBatch(p_tensor_vec<DType> input_data, p_tensor_vec<DType> label_data) {
        return 0;
    }

    template<typename DType>
    bool Network<DType>::SaveModel(const std::string &model_file) {
        return false;
    }

    template<typename DType>
    Phase Network<DType>::getPhase() const {
        return phase_;
    }

    template<typename DType>
    void Network<DType>::setPhase(Phase phase) {
        phase_ = phase;
    }

    // Private methods
    template<typename DType>
    p_tensor_vec<DType> Network<DType>::Forward(const p_tensor_vec<DType> &input_data) {
        return p_tensor_vec<DType>();
    }

    template<typename DType>
    float Network<DType>::Backward(const p_tensor_vec<DType> &label_data) {
        return 0;
    }

    template<typename DType>
    shared_ptr<Layer<DType>> Network<DType>::CreateLayerByType(const string &layer_type) {
        return shared_ptr<Layer<DType>>();
    }

    template<typename DType>
    string Network<DType>::LookaheadLayerType(const string &line) {
        return std::string();
    }

    INSTANTIATE_CLASS_NT(Network, (float)(double))

}