//
// Created by kobe on 7/24/19.
//

#include "optimizer.h"
namespace dnn {
    template<typename DType>
    void SGD<DType>::Update(p_tensor_vec<DType>& params, p_tensor_vec<DType>& gradients) {

    }

    template<typename DType>
    void SGDWithMomentum<DType>::Update(p_tensor_vec<DType>& params, p_tensor_vec<DType>& gradients) {

    }

    INSTANTIATE_CLASS_NT(SGD, (float)(double))
    INSTANTIATE_CLASS_NT(SGDWithMomentum, (float)(double))
}