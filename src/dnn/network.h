#ifndef FLY_DNN_NETWORK_H
#define FLY_DNN_NETWORK_H

#include "definition.h"
#include "tensor.h"
#include "utility/util.h"
#include "loss_function.h"
#include "optimizer.h"
#include "layers/layer.h"

#include <vector>

namespace dnn {
    template<typename DType>
    class Network {
    public:
        Network();

        virtual ~Network();

        /**
         * @brief Calculate batch loss.
         */

        float GetLoss(p_tensor_vec<DType> label_data, p_tensor_vec<DType> output_data);

        void SetInputSize(Shape shape);

        void SetLossFunctor(shared_ptr<LossFunctor<DType>> loss_functor);

        void SetOptimizer(shared_ptr<Optimizer<DType>> optimizer);

        void SetLearningRate(float lr);

        void AddLayer(shared_ptr<Layer<DType>> layer);

        float TrainBatch(p_tensor_vec<DType> input_data, p_tensor_vec<DType> label_data);

        bool SaveModel(const std::string &model_file);

    private:
        Phase getPhase() const;

        void setPhase(Phase phase);

        p_tensor_vec<DType> Forward(const p_tensor_vec<DType> &input_data);

        float Backward(const p_tensor_vec<DType> &label_data);

        shared_ptr<Layer<DType>> CreateLayerByType(const string &layer_type);

        string LookaheadLayerType(const string &line);

    private:
        Phase phase_{Phase::TRAIN};
        vector<shared_ptr<Layer<DType>>> layers_;
        p_tensor_vec<DType> data_;
        p_tensor_vec<DType> diff_;
        shared_ptr<LossFunctor<DType>> loss_functor_;
        shared_ptr<Optimizer<DType>> optimizer_;
    };

}; // namespace dnn

#endif //FLY_DNN_NETWORK_H