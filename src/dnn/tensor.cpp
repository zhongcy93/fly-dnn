/*
Copyright [2019] [kode]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * The Blob is used to represent dnn data model.
 */
#include "tensor.h"

namespace dnn {
    template<typename DType>
    Tensor<DType>::Tensor(const int batch, const int channels, const int height, const int width)
    :count_(0), capacity_(0){
        Init();
        Reshape(batch, channels, height, width);
    }

    template<typename DType>
    Tensor<DType>::Tensor(const Shape &shape) {
        Tensor(shape.nums_, shape.channel_, shape.height_, shape.width_);
    }


    template<typename DType>
    void Tensor<DType>::Init() {

    }

    template<typename DType>
    bool Tensor<DType>::Reshape(int batch, int channels, int height, int width) {
        Shape shape(batch, channels, height, width);
        return Reshape(shape);
    }

    template<typename DType>
    bool Tensor<DType>::Reshape(const Shape &shape) {
        count_ = shape.Size();
        shape_ = shape;
        if (count_ > capacity_) {
            capacity_ = count_;
            relocate();
        }
        return true;
    }

    template<typename DType>
    vector<DType> *Tensor<DType>::GetData() {
        return data_.get();
    }

    template<typename DType>
    void Tensor<DType>::Clear() {
        this->data_ = nullptr;
    }

    template<typename DType>
    void Tensor<DType>::Update() {

    }

    template<typename DType>
    Shape Tensor<DType>::GetShape() {
        return shape_;
    }

    template<typename DType>
    DType *Tensor<DType>::Pointer(const int n) {
        return &GetData()->at(offset(n));
    }

    template<typename DType>
    void Tensor<DType>::FillData(const DType item) {
        data_->assign(count_, item);
    }


    template<typename DType>
    void Tensor<DType>::CloneTo(std::shared_ptr<Tensor<DType>>& target){
        target = std::make_shared<Tensor<DType>>(*this);
    }


    //初始化不同的Tensor类，使得linker可以找到
    INSTANTIATE_CLASS_NT(Tensor, (float) (double) (int) (unsigned));
} // namespace dnn