//
// Created by kobe on 7/24/19.
//

#ifndef FLY_DNN_OPTIMIZER_H
#define FLY_DNN_OPTIMIZER_H

#include "tensor.h"

namespace dnn {
    template<typename DType>
    class Optimizer {
    public:
        Optimizer(const float lr) : lr_(lr) {};

        void SetLearningRate(const float lr) { lr_ = lr; };

        /**
         * @brief Update params by gradient.
         */
        virtual void Update(p_tensor_vec<DType>& params, p_tensor_vec<DType>& gradients) = 0;

    protected:
        float lr_;

    };

    template<typename DType>
    class SGD : public Optimizer<DType> {
    public:
        virtual void Update(p_tensor_vec<DType>& params, p_tensor_vec<DType>& gradients) override ;
    };


    template<typename DType>
    class SGDWithMomentum : public Optimizer<DType> {
    public:
        virtual void Update(p_tensor_vec<DType>& params, p_tensor_vec<DType>& gradients) override ;

    private:
        float momentum_ = 0.0f;
        p_tensor_vec<DType> prem_m_;
    };

}  // namespace dnn


#endif //FLY_DNN_OPTIMIZER_H
