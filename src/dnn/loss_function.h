#ifndef FLY_DNN_LOSS_FUNCTION_H
#define FLY_DNN_LOSS_FUNCTION_H

#include "tensor.h"

namespace dnn{
    template <typename DType>
    class LossFunctor{
        public:
            virtual float GetLoss(const p_tensor_vec<DType>& label_data, const p_tensor_vec<DType>& output_data) = 0;

            virtual float GetDiff(const p_tensor_vec<DType>& label_data, const p_tensor_vec<DType>& output_data) = 0;
    };

    template <typename DType>
    class CrossEntropyFunctor : public LossFunctor<DType>{
            virtual float GetLoss(const p_tensor_vec<DType>& label_data, const p_tensor_vec<DType>& output_data);

            virtual float GetDiff(const p_tensor_vec<DType>& label_data, const p_tensor_vec<DType>& output_data);
    };

    template <typename DType>
    class MSEFunctor : public LossFunctor<DType>{
            virtual float GetLoss(const p_tensor_vec<DType>& label_data, const p_tensor_vec<DType>& output_data);

            virtual float GetDiff(const p_tensor_vec<DType>& label_data, const p_tensor_vec<DType>& output_data);

    };
}  // namespce dnn

#endif //FLY_DNN_LOSS_FUNCTION_H