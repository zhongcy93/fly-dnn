/*
Copyright [2019] [kode]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef FLY_DNN_TENSOR_H
#define FLY_DNN_TENSOR_H

/**
 * 数据使用shared_ptr表示
 */

#include <memory>
#include <cassert>
#include "dnn/utility/util.h"
#include "macros.h"
#include "dnn/utility/logger.h"
#include "definition.h"


namespace dnn {

    template<typename DType>
    class Tensor {
    public:
        //Tips: 在函数声明时不使用const int，而是在函数定义的时候使用const int，只有在定义时使用const int才有效
        Tensor<DType>():count_(0), capacity_(0) {
            data_ = std::make_shared<vector<DType>>();
        };

        Tensor<DType>(int batch, int channels, int height, int width);

        explicit Tensor<DType>(const Shape &shape);

        void Init();

        bool Reshape(int batch, int channels, int height, int width);

        bool Reshape(const Shape &shape);

        vector<DType> *GetData();

        DType *Pointer(int n = 0);

        void Clear();

        void Update();

        Shape GetShape();

        int Nums() const {
            return shape_.nums_;
        }

        int Channel() const {
            return shape_.channel_;
        }

        int Height() const {
            return shape_.height_;
        }

        int Width() const {
            return shape_.width_;
        }

        uint_t offset(const int n = 0, const int c = 0, const int h = 0, const int w = 0) const {
            return n * (Channel() * Height() * Width()) + c * (Height() * Width()) + h * Width() + w;
        }

        // Read data by index
        DType &at(const int n = 0, const int c = 0, const int h = 0, const int w = 0) {
            return data_->at(offset(n, c, h, w));
        }

        DType at(const int n = 0, const int c = 0, const int h = 0, const int w = 0) const {
            return data_->at(offset(n, c, h, w));
        }

        DType &operator()(const int n = 0, const int c = 0, const int h = 0, const int w = 0) {
            return at(n, c, h, w);
        }

        DType operator()(const int n = 0, const int c = 0, const int h = 0, const int w = 0) const {
            return at(n, c, h, w);
        }

        // Place data to Vector by std::move
        void Place(vector<DType> &&data) {
            assert(data.size() == count_);
            data_ = make_shared<vector<DType>>(std::move(data));
        }

        void FillData(DType item);

        void CloneTo(std::shared_ptr<Tensor<DType>>& target);

    private:
        shared_ptr<vector<DType>> data_;
        Shape shape_;

        size_t count_;
        size_t capacity_;

        void relocate() {
            if (data_) data_->resize(capacity_);
        }
    };

} // namespace dnn

template<typename T>
using p_tensor_vec = std::vector<std::shared_ptr<dnn::Tensor<T>>>;

template <typename T>
using p_tensor_vecs = std::vector<std::shared_ptr<p_tensor_vec<T>>>;


#endif //FLY_DNN_TENSOR_H
