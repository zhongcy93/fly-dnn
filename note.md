# 笔记
## 知识点
1. static关键字  
在C++中，static关键字可以用在函数中，和在类中使用一样，仅仅初始化一次。但是这种用法在Java中不支持。  
2. 模板函数  
格式为：
```
template<typename T>
void fun(){}
```
如果这样定义函数，则调用时需要指定泛型类，但是如果使用：
```
template<typename... T>
```
调用时由于是可变模板参数则可以不必指定泛型。  
3. 宏的定义
可以参照如下例子：
```c++
// Define CHECK macro
#ifndef CHECK
#define CHECK(condition)\
        if(!(condition)) Logger::Fatal("Check failed:[" #condition\
        "] in file", __FILE__, "line", __LINE__);
#endif // CHECK

// Define ASSERT macro
#ifndef ASSERT
#define ASSERT(BOOL_SYMBOL, STR, ...) {\
            if(!(BOOL_SYMBOL)){\
                LOG_FATAL(STR, __VA_ARGS__);\
            }\
        }
#endif
```
4. std::forward\<T\>的使用
```C++
template<typename T>
static void Write(T &&t){
    Stream() << t << std::endl;
}

template<typename T, typename... Args>
static void Write(T &&t, Args &&... args){
    Stream() << t << ", ";
    Write(std::forward<Args>(args)...);
}
```
5. 类模板实例化
Caffe中之所以可以将实现和.h分开，是因为使用了相应的宏，宏中使用了显示实例化：
```C++
template class CLASSNAME<T>
```
或者使用
```C++
CLASSNAME<T> c;
```
隐式实例化。
Method:
```
template void swap<int>(int &a, int & b);  //显式实例化，只需声明
```
And  explicit specialization:
```
template<> void swap<job>(job &a, job &b)   //显式具体化（上面已经讲过，注意与实例化区分开，必须有定义）
{
  int salary:
  salary = a.salary:
  a.salary = b.salary;
  b.salary = salary;
};//explicite specialization.
```

6. 几个cast
    1. static_cast
        1. 原有的类型自动转换，例如short转int，子类转父类；转换失败报错    
        2. 指针转换，如int\* 转void\*
        3. 转换构造函数，如double转Complex
    2. dynamic_cast
        1. 向上转型
        2. 向下转型，需要有虚函数，转换失败结果为NULL
    3. const_cast  
        去const和volatile
    4. reinterpreter_cast  
        危险的操作，例如具体指针类型转换，int和指针间的转换

7. Const
    1. const修饰参数
    2. const修饰返回值，返回值不可修改
    3. const修饰成员函数，const对象仅可以调用const成员函数，函数内不能修改成员。